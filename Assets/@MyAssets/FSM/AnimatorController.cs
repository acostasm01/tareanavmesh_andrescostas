using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AnimatorController : MonoBehaviour
{
    private Animator animator;
    private NPCController characterController;
    private NavMeshAgent agent;
    private float speed;
    private float angularSpeed;

    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        animator = GetComponent<Animator>();
        characterController = GetComponent<NPCController>();

        animator.SetFloat("CycleOffset", Random.Range(0, 1));//para que las animaciones no est�n sincronizadas
        speed = Random.Range(3, 4);
        agent.speed = speed;
        angularSpeed = Random.Range(30, 40);
        agent.angularSpeed = angularSpeed;

        agent.updatePosition = false;
    }


    // Update is called once per frame
    void Update()
    {
        if (characterController.CharacterState == NPCController.State.Fleeing)
        {
            agent.speed = speed * 5;
            agent.angularSpeed = angularSpeed * 4;

            animator.SetInteger("Walking", 2);
        }
        else if((characterController.CharacterState == NPCController.State.Walking))
        {
            agent.speed = speed;
            agent.angularSpeed = angularSpeed;
            animator.SetInteger("Walking", 1);
        }
        else
        {
            animator.SetInteger("Walking", 0);

        }

        //para que el agente no se quede atr�s
        Vector3 worldDeltaPosition = agent.nextPosition - transform.position;
        if (worldDeltaPosition.magnitude > agent.radius)
        {
            transform.position = agent.nextPosition -0.9f * worldDeltaPosition;
        }

    }

    private void OnAnimatorMove()// se llama automaticamente
    {
        Vector3 position = animator.rootPosition;
        position.y = agent.nextPosition.y;
        transform.position = position;
    }


}
