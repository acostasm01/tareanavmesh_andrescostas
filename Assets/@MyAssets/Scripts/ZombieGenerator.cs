using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieGenerator : MonoBehaviour
{
    public List<GameObject> zombiePrefabs;
    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast(ray, out RaycastHit hit))
            {
                GameObject zombie = Instantiate(zombiePrefabs[Random.Range(0, zombiePrefabs.Count)]);
                zombie.transform.SetParent(transform, false);
                zombie.transform.position = hit.point;
            }
        }
    }
}
