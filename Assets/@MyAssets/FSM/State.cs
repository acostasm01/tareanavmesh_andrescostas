using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class State
{
    public enum STATE
    {
        HOME, WALK, FLEE
    };

    public enum EVENT
    {
        ENTER, UPDATE, EXIT
    };

    public STATE name;
    protected EVENT stage;
    protected State nextState;

    protected int minDistanceToZombie = 20;
    protected int walkSpeed = 10;
    protected int runSpeed = 20;

    protected GameObject safePlace;
    protected NavMeshAgent agent;//Navigation Agent from NPC
    protected GameObject goals;
    protected ZombieGenerator zombieGenerator;


    public State(int _minDistanciaToZombie,int _walkSpeed,int _runSpeed, GameObject _safePlace,
                NavMeshAgent _agent, GameObject _goals, ZombieGenerator _zombieGenerator)
    {
        minDistanceToZombie= _minDistanciaToZombie;
        walkSpeed= _walkSpeed;
        runSpeed= _runSpeed;
        safePlace= _safePlace;
        agent= _agent;
        goals= _goals;
        zombieGenerator = _zombieGenerator;
    
        stage = EVENT.ENTER;
    }

    public virtual void Enter()
    {
        stage = EVENT.UPDATE;
    }
    public virtual void Update()
    {
        stage = EVENT.UPDATE;
    }
    public virtual void Exit()
    {
        stage = EVENT.EXIT;
    }

    public State Process()
    {
       // Debug.Log(name);
        if (stage == EVENT.ENTER)
            Enter();
        if (stage == EVENT.UPDATE) 
            Update();
        if (stage == EVENT.EXIT)
        {
            Exit();
            return nextState;
        }
        return this;
    }


    public void RandomGoal()
    {
        
        agent.speed = walkSpeed;
        agent.SetDestination(goals.transform.GetChild(
               Random.Range(0, goals.transform.childCount)).position);
    }
    public void SafePositionGoal()
    {
        agent.speed = runSpeed;
        agent.SetDestination(safePlace.transform.position);
    }
    public bool GoalReached(int distance)
    {
        if (agent.remainingDistance < distance)
            return true;
        else
            return false;
    }

    public Vector3 GetNextPosition()
    {
        return agent.nextPosition;
    }

    public float GetRadius()
    {
        return agent.radius;
    }


    public Transform IsAnyZombieNear(GameObject npc)
    {
        foreach (Transform child in zombieGenerator.transform)
        {
            if (Vector3.Distance(child.position, npc.transform.position)
                < minDistanceToZombie)
            {
                return child;//devolvemos la referencia al zombie
                //se podr�a hacer con un rayo (linea de visi�n)
            }
        }
        return null;
    }
}
