using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PeopleGenerator : MonoBehaviour
{
    public List<GameObject> peoplePrefabs;

    int maxPeople = 10;

    Vector2 inicialPosition = new Vector2 (-20, 20);
    // Start is called before the first frame update
    void Start()    
    {
        for (int i = 0; i < maxPeople; i++)
        {
            GameObject person = peoplePrefabs[Random.Range(0,5)];
            person.transform.position = new Vector3(Random.Range(inicialPosition.x, inicialPosition.y),
                0, Random.Range(inicialPosition.x, inicialPosition.y));
            Instantiate(person);
        }
    }
}
