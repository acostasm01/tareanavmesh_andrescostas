using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public abstract class NPCController : MonoBehaviour
{
    protected NavMeshAgent agent;

    protected GameObject goals;

    public enum State
    {
        SafePlace,
        Walking,
        Fleeing
    }
    // Start is called before the first frame update
    public State CharacterState
    {
        get; set;
    }
    public void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        goals = GameObject.FindGameObjectWithTag("goals");

        agent.SetDestination(
            goals.transform.GetChild(Random.Range(0, goals.transform.childCount)).position);
        CharacterState = State.Walking;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
