using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Home : State
{

    private GameObject NPC;//Non Playing Character is executing the FSM
   // private FSM fsm;



    public Home(GameObject _npc, int _minDistanciaToZombie, int _walkSpeed, int _runSpeed, GameObject _safePlace,
                NavMeshAgent _agent, GameObject _goals, ZombieGenerator _zombieGenerator):
            base(_minDistanciaToZombie, _walkSpeed, _runSpeed, _safePlace, _agent, _goals, _zombieGenerator)
    {
        NPC = _npc;
        //   fsm = npc.GetComponent<FSM>();
        name = STATE.HOME; // Set name of current state.
    }

    public override void Enter()
    {
        base.Enter(); // Sets stage to UPDATE.

    }

    // Update is called once per frame
    public override void Update()
    {
        Transform zombie = IsAnyZombieNear( NPC);

        if (zombie == null)
        {
            nextState = new Walk(NPC, minDistanceToZombie, walkSpeed, runSpeed, safePlace, agent, goals, zombieGenerator);
            stage = EVENT.EXIT; //The next time Process runs, the EXIT stage will run instead, which will then return the nextState
            RandomGoal();
        }
    }

    public override void Exit()
    {
        base.Exit();
    }

   

}
