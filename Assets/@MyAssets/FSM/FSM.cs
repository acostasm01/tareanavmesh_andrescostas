using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class FSM : MonoBehaviour
{  
    public  int minDistanceToZombie = 20;
    public int walkSpeed = 10;
    public int runSpeed = 20;

    private GameObject safePlace;
    private NavMeshAgent agent;//Navigation Agent from NPC
    private GameObject goals;
    private ZombieGenerator zombieGenerator;

    State currentState;

    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        //tengo que recogerlos porque al estar en un prefab en assets no puedo asignarlos a trav�s del inspector
        zombieGenerator = GameObject.FindObjectOfType<ZombieGenerator>();
        safePlace = GameObject.FindGameObjectWithTag("safePlace");
        goals = GameObject.FindGameObjectWithTag("goals");


        currentState = new Walk(this.gameObject,minDistanceToZombie,walkSpeed,runSpeed
            ,safePlace,agent,goals,zombieGenerator); // Create our first state.
    }

    void Update()
    {
        currentState = currentState.Process(); // Calls Process method to ensure correct state is set.
    }

    public State GetCurrentState()
    {
        return currentState;
    }

   
}
