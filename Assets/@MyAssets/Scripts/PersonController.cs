using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonController : NPCController
{
    private GameObject safePlace;
    private Transform zombie;
    // Start is called before the first frame update
    void Start()
    {
        base.Start();
        safePlace = GameObject.FindGameObjectWithTag("safePlace");
        zombie = isAnyZombieNearby();
    }

    // Update is called once per frame
    void Update()
    {
        zombie = isAnyZombieNearby();

        if (zombie != null)
        {
            if (agent.destination != safePlace.transform.position)
            {
                agent.SetDestination(safePlace.transform.position);
                CharacterState = State.Fleeing;
                Debug.Log("destination safeplace");
            }
            else
            {
                if (agent.remainingDistance < 4)
                {
                    CharacterState = State.SafePlace;
                    Debug.Log("esta en safeplace");
                }
            }
        }
        else
        {
            CharacterState = State.Walking;
            agent.speed = 10;
            if (agent.remainingDistance < 1)
            {
                agent.SetDestination(goals.transform.GetChild(Random.Range(0, goals.transform.childCount)).position);
                Debug.Log(agent.remainingDistance);
            }
        }
    }
    private Transform isAnyZombieNearby()
    {
        GameObject[] zombies = GameObject.FindGameObjectsWithTag("zombie");
        foreach(GameObject zombie in zombies)
        {
            if( Vector3.Distance(transform.position, zombie.transform.position) < 5)
            {
                return zombie.transform;
            }
        }
        return null;
    }
}
