using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZombieController : NPCController
{
    // Start is called before the first frame update
    void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        if(agent.remainingDistance < 4)
        {
            agent.SetDestination(goals.transform.GetChild(Random.Range(0, goals.transform.childCount)).position);
        }
    }
}
