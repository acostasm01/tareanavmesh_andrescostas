using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class AnimatorControllerFSM : MonoBehaviour
{
    private Animator animator;
    FSM fsm;

    // Start is called before the first frame update
    void Start()
    {
        fsm = GetComponent<FSM>();
        animator = GetComponent<Animator>();
        animator.SetFloat("CycleOffset", UnityEngine.Random.Range(0, 1));//para que las animaciones no est�n sincronizadas
    }
    // Update is called once per frame
    void Update()
    {

        Type stateType = fsm.GetCurrentState().GetType();

        animator.SetInteger("Walking", 1);
        if (stateType.Equals(typeof(Flee)))
        {
            animator.SetInteger("Walking", 2);
        }
        else if (stateType.Equals(typeof(Walk)))
        {
            animator.SetInteger("Walking", 1);
        }
        else
        {
            animator.SetInteger("Walking", 0);
        }

        //para que el agente no se quede atr�s
        Vector3 worldDeltaPosition = fsm.GetCurrentState().GetNextPosition() - transform.position;
        if (worldDeltaPosition.magnitude > fsm.GetCurrentState().GetRadius())
        {
            transform.position = fsm.GetCurrentState().GetNextPosition() - 0.9f * worldDeltaPosition;
        }
    }
    private void OnAnimatorMove()// se llama automaticamente
    {
        Vector3 position = animator.rootPosition;
        position.y = fsm.GetCurrentState().GetNextPosition().y;
        transform.position = position;
    }

}
